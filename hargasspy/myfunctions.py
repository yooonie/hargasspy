import requests

url = "https://web.hargassner.at/api"
auth = "/auth/login"
refresh = "/auth/refresh"
instal = "/installations"

def login(email, password, clientid, secret):
    payload = "{\"email\":\"" + email + "\",\"password\":\"" + password + "\",\"client_id\":\"" + clientid + "\",\"client_secret\":\"" + secret + "\"}"
    headers = {'Content-Type': 'application/json'
    }
    print(requests.request("POST", url + auth, data=payload, headers=headers).text)

def retoken(email, refresh_token, clientid, secret):
    payload = "{\"email\":\"" + email + "\",\"refresh_token\":\"" + refresh_token + "\",\"client_id\":\"" + clientid + "\",\"client_secret\":\"" + secret + "\"}"
    headers = {'Content-Type': 'application/json'}
    print(requests.request("POST", url + refresh, data=payload, headers=headers).text)

def installation(token):
    payload = ""
    headers = {
    'Content-Type': "application/json",
    'Authorization': "Bearer " + token + ""
    }
    #print(requests.request("GET", url + instal, data=payload, headers=headers).text)
    response = requests.request("GET", url + instal, data=payload, headers=headers)
    #print(response.status_code)
    #print(response.text)
    data = response.json()
    #print(data['data'][0]['name'],data['data'][0]['devices'] )
    print(data['data'][0]['id'] + response.status_code)
    #print(data)

def widgets(id, token):
    payload = ""
    headers = {
    'Content-Type': "application/json",
    'Authorization': "Bearer " + token + ""
    }
    print(requests.request("GET", url + instal + "/" + id + "/widgets", data=payload, headers=headers).text)
    
def update(id, token, value, types, number):
    payload = "{\"value\":\"" + value + "\"}"
    headers = {
    'Content-Type': "application/json",
    'Authorization': "Bearer " + token + ""
    }

    print(requests.request("PATCH", url  + instal + "/" + id + "/widgets/" + types + "/" + number + "/parameters/mode" , data=payload, headers=headers).text)


#login("mail@gmail.com","password","1","user_secret")
#retoken("mail@gmail.com","refresh_token","1","user_secret")
installation("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYTQ5MTM0MmQ4NDk0Y2VkYjcxOWE4YWRiMGYzYjlmNjZjMjk2M2FhNjY5ODFiNWIzNWU4YTc4MWFjMWM0YzQ4ZWI4NDE2ZjNjNjgyMmJjYWQiLCJpYXQiOjE2MDU2NDI4NjUsIm5iZiI6MTYwNTY0Mjg2NSwiZXhwIjoxNjA1NjQ2NDY1LCJzdWIiOiIxODUyMiIsInNjb3BlcyI6W119.XnFvrvtI25v2pc3It2RClswJ7KZZaoYeiRl1MLlfddlxBIOvvlSwlbJfwm_sHjxHDr5DWr4rWT4lsxqN74VETJ1F7WyxyXWYGPeeMTweL5mMi6JVI1wmj4oRMyTkLhSApE0Tb-w5nrqirsmLO2pwsoEKM1zKVBHOkHXq0T47qptpARIxUNuFj3uaSExGsmGVAU-Z-fkwKvnblTjA9qK26TTJKI4P2Ol-37Tsu_7CsEm4t3rajCIes7dk13z8Ix5cglQz_4qbQAOn_1BkIAqt_duCeUBEMejMeqimQPBbTuXsIK2MaJ6DlOk3D1whXIK1-_iZ7cXOR0jCaTNifHW-LAiXV-K-bE8qZQ-c8CeKk2BxS5YNkHUDWxvZDspFBt6DZ6PdLv68M_Bo23AGvSA-UfpPhRxpepKD4C9jy_mdYNF9ukbFb3EubCxXggGDIHFqvvmCA4QIBLyKbBAEt2JUo1ojRVjtkSTun8a0P53VkPHCYMHkl_TVY-0hhUpXvQLdhj5j5Ux1DKixRxMYOJldWK4xN_6c4gGkzOmNECS2pa42BX507PAhwzQk2c693rW9oRpmhOTGu1c_Ky1vKfuAsFHKa7S4xZePLtm-NXw7bslBibmjiD1sagccNX28M3hgWsMJK1QCKFXG-J04xKYqSPTM-AbOBSxRtvnANTlh1iI")
#widgets("11390","eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZTk5Y2QyMWE0YjhjZDQwZTkyMGYxNzdkMGU3ODY2ZDg2OWM0NGU1NzQyYWZlOGFmNGVlZmYzMTMwZWNjZjQ0NTBjM2JjMWVmNzBkYzRkMjIiLCJpYXQiOjE2MDU2MDYxMTcsIm5iZiI6MTYwNTYwNjExNywiZXhwIjoxNjA1NjA5NzE3LCJzdWIiOiIxODUyMiIsInNjb3BlcyI6W119.KAbtDJsd5gHBsjU42QHL9iDBFwNZMygH_6oIkcAhvM4YroRi7-axUi90aq6izml8E-QV8tx_ERVyJvS5PFR59KYjpBvyKdrwLJdhxocwa0s-xmcLg26f86zF9g8Rj-fgNgnemQpjdOPowSS96UIYzZ-keKAG87_bIr6a5If2JlTR3l2uIxiGnP58-4s57FTlbu5zzWB59RaCprqDsSbs-VPAPrWSb0EBPFKBOT5TLfp-1IKj3kv7pIgUUA_e6sXilqZdeIBKN4r1y2FM5btH5Tgja2YkZgHw2OxMgSLM5vbXYbx9Q6UldA0vW1PczRCyLTXFHs1947tnk95DyRU3Xa6J09gIrMZVXWNrPlOzmjbonLhAQ77VjTZRt32mvDUVVt0P_DUw6z3DNsj-Cp_qWtRjqsAJGHu83JUIMMKaQpzrXHKpD5oeOQtK3iD64R4Te9lpd7fiC1js0UCKvLqVhFeagonCmbFZv8GpdLIAUD-qv-NEnr3cIXu4MKm1z9l32mSClaFFiKmmgYScNd8LNBhwi8HAIWvqc28VM8LJYqdKB1rGoOOt14Bmz-9MmpK6R-51LxQoINWe__UxDWkyOfUwvxDtHT1qstPcVMYb9DxKASmRszyrvv8sQtLHqC_GVJJwFT08ahpWHfktxlbqw4HfgWcOIIvDif_Z0lia9B8")
#update("11390","eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZTk5Y2QyMWE0YjhjZDQwZTkyMGYxNzdkMGU3ODY2ZDg2OWM0NGU1NzQyYWZlOGFmNGVlZmYzMTMwZWNjZjQ0NTBjM2JjMWVmNzBkYzRkMjIiLCJpYXQiOjE2MDU2MDYxMTcsIm5iZiI6MTYwNTYwNjExNywiZXhwIjoxNjA1NjA5NzE3LCJzdWIiOiIxODUyMiIsInNjb3BlcyI6W119.KAbtDJsd5gHBsjU42QHL9iDBFwNZMygH_6oIkcAhvM4YroRi7-axUi90aq6izml8E-QV8tx_ERVyJvS5PFR59KYjpBvyKdrwLJdhxocwa0s-xmcLg26f86zF9g8Rj-fgNgnemQpjdOPowSS96UIYzZ-keKAG87_bIr6a5If2JlTR3l2uIxiGnP58-4s57FTlbu5zzWB59RaCprqDsSbs-VPAPrWSb0EBPFKBOT5TLfp-1IKj3kv7pIgUUA_e6sXilqZdeIBKN4r1y2FM5btH5Tgja2YkZgHw2OxMgSLM5vbXYbx9Q6UldA0vW1PczRCyLTXFHs1947tnk95DyRU3Xa6J09gIrMZVXWNrPlOzmjbonLhAQ77VjTZRt32mvDUVVt0P_DUw6z3DNsj-Cp_qWtRjqsAJGHu83JUIMMKaQpzrXHKpD5oeOQtK3iD64R4Te9lpd7fiC1js0UCKvLqVhFeagonCmbFZv8GpdLIAUD-qv-NEnr3cIXu4MKm1z9l32mSClaFFiKmmgYScNd8LNBhwi8HAIWvqc28VM8LJYqdKB1rGoOOt14Bmz-9MmpK6R-51LxQoINWe__UxDWkyOfUwvxDtHT1qstPcVMYb9DxKASmRszyrvv8sQtLHqC_GVJJwFT08ahpWHfktxlbqw4HfgWcOIIvDif_Z0lia9B8", "MODE_AUTOMATIC", "heating-circuits", "1")
