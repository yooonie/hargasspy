from setuptools import find_packages, setup
setup(
    name='hargasspy',
    packages=find_packages(),
    version='0.0.1',
    description='Hargassner api library',
    author='@yooonie',
    license='MIT',
)


import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="hargasspy", # Replace with your own username
    version="0.0.1",
    author="@yooonie",
    author_email="yooonie@gmail.com",
    description="library to manage hargassner boiler who have internet gateway",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/yooonie/hargasspy",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)